---
title: 'Figma'
description: "Figma is the most gadget designer of the world"
publishDate: 'Monday, August 30 2021'
author: 'Antonio Arias'
heroImage: 'https://i0.wp.com/hipertextual.com/wp-content/uploads/2019/03/hipertextual-cursos-online-aprender-javascript-2019180345.jpg?w=1500&ssl=1'
layout: '../../layouts/BlogPost.astro'
---

## What is Figma?

Figma is a good tool of design, very much easy to use, this tool was made to create design layouts of sites web, include to  generate code for hand-off projects, and illustrate.