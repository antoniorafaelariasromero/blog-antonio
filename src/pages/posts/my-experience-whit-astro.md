---
title: 'My experience whit Astro'
description: "Astro is a very interesting 'Static Page Generator', because with it you can make 'Blogs, Documentation and Portfolio'"
publishDate: 'Sunday, August 29 2021'
author: 'Antonio Arias'
heroImage: '/social.jpg'
alt: 'Astro'
layout: '../../layouts/BlogPost.astro'
---

## What is Astro?

Astro is a **Static Site Generator** very especial, since it use whit different frameworks, with which the you can generates blogs, documentation and more, with most easy possible, whith a structure defined.

## Use

To init **Astro** need directory:

``` js
mkdir example-project
cd example-project
```

And init project whit the comands

```js
npm init astro
npm i
```

To init serve in eleventy eject comand
```js
npm run dev
```