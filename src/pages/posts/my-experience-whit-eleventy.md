---
title: 'My experience whit Eleventy'
description: "Eleventy like Astro"
publishDate: 'Monday, August 30 2021'
author: 'Antonio Arias'
heroImage: '/eleventy.jfif'
alt: 'Astro'
layout: '../../layouts/BlogPost.astro'
---

## What is eleventy?

Eleventy is a simple **Static Site Generator**, with which the you can generates blogs, documentation and more, with most easy posible.

## Use

To init **eleventy** need directory:

``` js
mkdir example-project
cd example-project
```

And init project whit the comands

```js
npm init -y
npm i --save-dev @11ty/eleventy
npx @11ty/eleventy OR eleventy
```

To init serve in eleventy eject comand
```js
npx @11ty/eleventy --serve OR eleventy --serve
```
